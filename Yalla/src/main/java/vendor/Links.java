package vendor;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Links {
public static void main (String [] args){
	 System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
	 ChromeOptions op = new ChromeOptions();
	 op.addArguments("--disable-notifications");
	 ChromeDriver driver = new ChromeDriver(op);
// To maximize the window
driver.manage().window().maximize();
// Open application
driver.get("https://www.google.co.in/?gws_rd=ssl#q=softwaretestingmaterial.com");
// Get the list of all links

List<WebElement> link = driver.findElements(By.xpath("(//*[span/h3/a])[6]"));
// Using for loop to display the text of all the links
for(WebElement element : link)
{
System.out.println(element.getText());
}
// Click on the first link
driver.findElement(By.xpath("(//*[span/h3/a])[1]")).click();
}
}


