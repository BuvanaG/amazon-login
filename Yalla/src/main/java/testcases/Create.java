package testcases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
//@Listeners(listeners.Create)
public class Create extends Annotations {
	@BeforeTest(groups="smoke")   
	public void setData() {
		testcaseName= "TC001_CreateLead";
		testcaseDec = "Create a new Lead in leaftaps";
		author      = "Gayatri";
		category    = "Smoke";
	}
	@Test(groups="smoke",dataProvider="fetchData")
	public void createLead(String Cname,String Fname,String Lname) {
		click(locateElement("link", "CRM/SFA"));
		click(locateElement("link", "Leads"));
		click(locateElement("link", "Create Lead"));
		clearAndType(locateElement("id", "createLeadForm_companyName"), "Cname");
		clearAndType(locateElement("id", "createLeadForm_firstName"), "Fname");
		clearAndType(locateElement("id", "createLeadForm_lastName"), "Lname");
		click(locateElement("name", "submitButton"));
		}
		@DataProvider(name="createData")
		public Object[] fetchData()  {
		Object data[][]=new Object[2][3];
		data[0][0]="Leaf";
		data[0][1]="uva";
		data[0][2]="g";
		data[1][0]="Leaftest";
		data[1][1]="uvani";
		data[1][2]="N";
		return data;
		
		
		
		
	}
	
}

