package testcases;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.yalla.selenium.api.base.SeleniumBase;

public class Login extends SeleniumBase{
	@BeforeMethod(groups="sanity",dependsOnMethods="smoke")
	public void login() throws InterruptedException {
		/*startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		clearAndType(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement hr = locateElement("link","CRM/SFA");
		click(hr);*/
		WebElement cr = locateElement("link","Create Lead");
		click(cr);
		WebElement src = locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingIndex(src, 12);
		WebElement crs = locateElement("link","Find Leads");
		click(crs);
		WebElement crm = locateElement("link","Milan");
		click(crm);
		/*WebElement crk = locateElement("link","Edit");
		click(crk);
		WebElement upt = locateElement("id","updateLeadForm_companyName");
		clearAndType(upt,"hp");
		Thread.sleep(2000);
		WebElement sub = locateElement("xpath","//input[@name='submitButton']");
		click(sub);*/
		WebElement dup = locateElement("link","Duplicate Lead");
		click(dup);
	     WebElement vai = locateElement("xpath","//input[@name='submitButton']");
		click(vai);
		WebElement del = locateElement("link","Delete");
		click(del);
		WebElement mr = locateElement("link","Merge Leads");
		click(mr);
		/*WebElement xp = locateElement("xpath","//img[@alt='Lookup']");
		click(xp);
		switchToWindow(1);
		WebElement etext = locateElement("xpath", "//input[@name='id']");
		clearAndType(etext, "10044");
		WebElement tbutton = locateElement("xpath","//button[text()='Find Leads']");
	    click(tbutton);
		Thread.sleep(2000);
         WebElement mrl = locateElement("link","10044");
		click(mrl);
		switchToWindow(0);
		WebElement xps = locateElement("xpath","(//img[@alt='Lookup'])[2]");
		click(xps);
		switchToWindow(1);
		WebElement etex = locateElement("xpath", "//input[@name='id']");
		clearAndType(etex, "10046");
        WebElement tbuttons = locateElement("xpath","//button[text()='Find Leads']");
	    click(tbuttons);
		Thread.sleep(2000);
        WebElement mrb = locateElement("link","10046");
		click(mrb);
		switchToWindow(0);
	WebElement mrm = locateElement("link","Merge");
		click(mrm);
		acceptAlert();
		switchToFrame(1);
		/*WebElement el = locateElement("xpath","//div[text()='No records to display']");
		
		String s=el.getText();
		if(s.equalsIgnoreCase("No records to display"))
		{
			System.out.println("sucessfully displayed");
		}
		
		Thread.sleep(2000);*/
		takeSnap();
		
		/*if(dup.isDisplayed()&&crk.isDisplayed()&&mrm.isDisplayed()) {
			System.out.println("click all leads");
		}
		else {
	    
	    System.out.println("some error");
}*/

	}
}




