package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import com.yalla.selenium.api.base.SeleniumBase;

public class UserLogin extends SeleniumBase {
	@Parameters({"url","username","password"})
	  //@BeforeMethod(groups="smoke")
	@BeforeMethod
	public void beforeMethod(String url,String username,String password) {
		  startApp("chrome", url);
			WebElement eleUserName = locateElement("id", "username");
			clearAndType(eleUserName, username);
			WebElement elePassword = locateElement("id", "password");
			clearAndType(elePassword, password);
			WebElement eleLogin = locateElement("class", "decorativeSubmit");
			click(eleLogin);
			WebElement hr = locateElement("link","CRM/SFA");
			click(hr);

}
}
