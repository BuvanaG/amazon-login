package testcases;

import org.testng.annotations.Test;
import com.yalla.selenium.api.base.SeleniumBase;

import utils.DataLibrary;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.AfterMethod;

import java.io.IOException;

import org.openqa.selenium.WebElement;


public class Annotations extends SeleniumBase{
	//@Parameters({"url","username","password"})
  //@BeforeMethod(groups= {"any"})
	@BeforeMethod
  public void beforeMethod() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		clearAndType(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
	  }
	@DataProvider(name="fetchdata")
	public Object[][] fetchdata() throws IOException {
		return DataLibrary.readExcelData(excelFileName);
		
	/*Object[][] data = new Object[2][3];
		data[0][0] = "TestLeaf";
		data[0][1] = "Sarath";
		data[0][2] = "M";
		data[1][0] = "TestLeaf";
		data[1][1] = "Kaushik";
		data[1][2] = "S";
		return data;*/
		
			}
	
  

  //@AfterMethod(groups= {"any"})
  @AfterMethod
  public void afterMethod() {
	  close();
	  
  }

 
}
